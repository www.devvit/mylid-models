<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Mylid\SharedModels\Owner
 *
 * @property int $id
 * @property int $userId
 * @property Carbon $created_at
 * @property-read Collection|Company[] $companies
 * @property-read int|null $companies_count
 * @property-read Collection|PaymentHistory[] $paymentHistory
 * @property-read int|null $payment_history_count
 * @method static Builder|Owner newModelQuery()
 * @method static Builder|Owner newQuery()
 * @method static Builder|Owner query()
 * @method static Builder|Owner whereUserId($value)
 * @method static Builder|Owner whereCreatedAt($value)
 * @method static Builder|Owner whereId($value)
 * @mixin \Eloquent
 */
class Owner extends Model
{
    protected $table = 'owners';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = false;

    public function companies()
    {
        return $this->hasMany(Company::class);
    }

    public function paymentHistory()
    {
        return $this->hasMany(PaymentHistory::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'email_verified_at' => 'datetime',
    ];
}
