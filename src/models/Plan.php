<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Model;

/**
 * Mylid\Database\Plan
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int|null $price_1month
 * @property int|null $price_3month
 * @property int|null $price_6month
 * @property int|null $price_12month
 * @property \Illuminate\Support\Carbon $created_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mylid\Database\PlanPermission[] $permissions
 * @property-read int|null $permissions_count
 * @method static \Illuminate\Database\Eloquent\Builder|Plan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plan query()
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan wherePrice12month($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan wherePrice1month($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan wherePrice3month($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan wherePrice6month($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereSlug($value)
 * @mixin \Eloquent
 */
class Plan extends Model
{

  protected $table = 'pricing-plans';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [];

    public $timestamps = false;

  /**
   * Get the user that owns the phone.
   */
  public function permissions()
  {
    return $this->belongsToMany(PlanPermission::class, 'plans_plan-permissions');
  }

  public function companies()
  {
    return $this->hasMany(Company::class);
  }

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    //   'phone_verified_at' => 'datetime',
  ];
}
