<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Model;

/**
 * Mylid\Database\PlanPermission
 *
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mylid\Database\Plan[] $plans
 * @property-read int|null $plans_count
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPermission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPermission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPermission query()
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPermission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPermission whereName($value)
 * @mixin \Eloquent
 */
class PlanPermission extends Model
{
    
    protected $table = 'plan-permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public $timestamps = false;

    /**
     * Get the user that owns the phone.
     */
    public function plans()
    {
        return $this->belongsToMany(Plan::class, 'plans_plan-permissions');
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    //   'phone_verified_at' => 'datetime',
    ];
}