<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Model;

/**
 * Mylid\Database\SmsCode
 *
 * @property int $id
 * @property string $phone
 * @property string $code
 * @property string $sent_at
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode whereSentAt($value)
 * @mixin \Eloquent
 */
class SmsCode extends Model
{

    protected $table = 'sms_codes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}