<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Mylid\SharedModels\EmployeeSchedule
 *
 * @property int $id
 * @property string $schedule_date
 * @property string $time_start
 * @property string $time_end
 * @property string|null $break_start_time
 * @property string|null $break_end_time
 * @property int $employee_id
 * @property int $company_id
 * @property-read Company $company
 * @property-read Employee $employee
 * @method static Builder|EmployeeSchedule newModelQuery()
 * @method static Builder|EmployeeSchedule newQuery()
 * @method static Builder|EmployeeSchedule query()
 * @method static Builder|EmployeeSchedule whereBreakEndTime($value)
 * @method static Builder|EmployeeSchedule whereBreakStartTime($value)
 * @method static Builder|EmployeeSchedule whereCompanyId($value)
 * @method static Builder|EmployeeSchedule whereEmployeeId($value)
 * @method static Builder|EmployeeSchedule whereId($value)
 * @method static Builder|EmployeeSchedule whereScheduleDate($value)
 * @method static Builder|EmployeeSchedule whereTimeEnd($value)
 * @method static Builder|EmployeeSchedule whereTimeStart($value)
 * @mixin \Eloquent
 */
class EmployeeSchedule extends Model
{
    protected $table = 'employee-schedules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * Get the user that owns the phone.
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}