<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Model;

/**
 * Mylid\Database\Service
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int|null $group_id
 * @property int $company_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mylid\Database\Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @property-read \Mylid\Database\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mylid\Database\Employee[] $employees
 * @property-read int|null $employees_count
 * @method static \Illuminate\Database\Eloquent\Builder|Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereName($value)
 * @mixin \Eloquent
 */
class Service extends Model
{

    protected $table = 'services';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * Get the user that owns the phone.
     */
    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'employees_services')
            ->using(EmployeeService::class)
            ->withPivot([
                'id',
                'employee_id',
                'service_id',
                'company_id',
                'price',
                'duration',
                'book_step'
            ]);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function serviceGroup()
    {
        return $this->belongsTo(ServiceGroup::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}