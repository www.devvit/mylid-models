<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Model;


/**
 * Mylid\Database\PaymentHistory
 *
 * @property int $id
 * @property int $owner_id
 * @property int $company_id
 * @property string $info
 * @property int $amount
 * @property string|null $paid_at
 * @property int $status_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property-read \Mylid\Database\Owner $owner
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentHistory whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentHistory whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentHistory whereInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentHistory whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentHistory wherePaidAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentHistory whereStatusId($value)
 * @mixin \Eloquent
 */
class PaymentHistory extends Model
{

    protected $table = 'payment-history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}