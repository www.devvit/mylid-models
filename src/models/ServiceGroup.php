<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Model;

/**
 * Mylid\Database\ServiceGroup
 *
 * @property int $id
 * @property string $name
 * @property int $company_id
 * @property-read \Mylid\Database\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mylid\Database\Service[] $services
 * @property-read int|null $services_count
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceGroup whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceGroup whereName($value)
 * @mixin \Eloquent
 */
class ServiceGroup extends Model
{

    protected $table = 'service-groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * Get the user that owns the phone.
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}
