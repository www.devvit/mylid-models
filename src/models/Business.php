<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Mylid\SharedModels\Business
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property Carbon $created_at
 * @property-read Collection|Company[] $companies
 * @property-read int|null $companies_count
 * @method static Builder|Business newModelQuery()
 * @method static Builder|Business newQuery()
 * @method static Builder|Business query()
 * @method static Builder|Business whereCreatedAt($value)
 * @method static Builder|Business whereId($value)
 * @method static Builder|Business whereName($value)
 * @method static Builder|Business whereSlug($value)
 * @mixin \Eloquent
 */
class Business extends Model
{

    protected $table = 'businesses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * Get the user that owns the phone.
     */
    public function companies()
    {
        return $this->hasMany(Company::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}
