<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Mylid\SharedModels\Company
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $open_at
 * @property string|null $close_at
 * @property string|null $timezone
 * @property int $business_id
 * @property int $owner_id
 * @property Carbon $created_at
 * @property string|null $paid_until
 * @property int|null $pricing_plan_id
 * @property-read Collection|Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @property-read Business $business
 * @property-read Collection|Employee[] $employees
 * @property-read int|null $employees_count
 * @property-read Owner $owner
 * @method static Builder|Company newModelQuery()
 * @method static Builder|Company newQuery()
 * @method static Builder|Company query()
 * @method static Builder|Company whereBusinessId($value)
 * @method static Builder|Company whereCloseAt($value)
 * @method static Builder|Company whereCreatedAt($value)
 * @method static Builder|Company whereId($value)
 * @method static Builder|Company whereName($value)
 * @method static Builder|Company whereOpenAt($value)
 * @method static Builder|Company whereOwnerId($value)
 * @method static Builder|Company wherePaidUntil($value)
 * @method static Builder|Company wherePricingPlanId($value)
 * @method static Builder|Company whereSlug($value)
 * @method static Builder|Company whereTimezone($value)
 * @mixin \Eloquent
 */
class Company extends Model
{

    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}