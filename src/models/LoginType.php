<?php

namespace Mylid\SharedModels;

class LoginType 
{
  const OWNER = 'owner';
  const EMPLOYEE = 'employee';
  const CLIENT = 'client';
  public static $types = [self::OWNER, self::EMPLOYEE, self::CLIENT];
}