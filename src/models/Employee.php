<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Mylid\SharedModels\Employee
 *
 * @property int $id
 * @property int $company_id
 * @property string|null $phone
 * @property string|null $email
 * @property string $firstname
 * @property string $lastname
 * @property string|null $middlename
 * @property string|null $birth
 * @property string|null $img_path
 * @property string|null $img_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @property-read Company $company
 * @property-read Collection|Job[] $jobs
 * @property-read int|null $jobs_count
 * @property-read Collection|EmployeeSchedule[] $schedule
 * @property-read int|null $schedule_count
 * @property-read Collection|Service[] $services
 * @property-read int|null $services_count
 * @method static Builder|Employee newModelQuery()
 * @method static Builder|Employee newQuery()
 * @method static Builder|Employee query()
 * @method static Builder|Employee whereActive($value)
 * @method static Builder|Employee whereBirth($value)
 * @method static Builder|Employee whereCompanyId($value)
 * @method static Builder|Employee whereCreatedAt($value)
 * @method static Builder|Employee whereEmail($value)
 * @method static Builder|Employee whereFirstname($value)
 * @method static Builder|Employee whereId($value)
 * @method static Builder|Employee whereImgName($value)
 * @method static Builder|Employee whereImgPath($value)
 * @method static Builder|Employee whereLastname($value)
 * @method static Builder|Employee whereMiddlename($value)
 * @method static Builder|Employee wherePassword($value)
 * @method static Builder|Employee wherePhone($value)
 * @method static Builder|Employee whereToken($value)
 * @method static Builder|Employee whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Employee extends Model
{
    protected $table = 'employees';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * Get the user that owns the phone.
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function schedule()
    {
        return $this->hasMany(EmployeeSchedule::class);
    }

    public function jobs()
    {
        return $this->belongsToMany(Job::class, 'employees_jobs');
    }

    public function services()
    {
        return $this->belongsToMany(Service::class, 'employees_services')
            ->using(EmployeeService::class)
            ->withPivot([
                'id',
                'employee_id',
                'service_id',
                'company_id',
                'price',
                'duration',
                'book_step'
            ]);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}
