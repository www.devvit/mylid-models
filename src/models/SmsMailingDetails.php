<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Model;

/**
 * Mylid\Database\SmsMailingDetails
 *
 * @property int $id
 * @property string|null $sms_id
 * @property int|null $client_id
 * @property string $phone
 * @property int|null $status_code
 * @property int $sms_mailing_id
 * @property-read \Mylid\Database\Client|null $client
 * @property-read \Mylid\Database\SmsMailing $smsMailing
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailingDetails newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailingDetails newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailingDetails query()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailingDetails whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailingDetails whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailingDetails wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailingDetails whereSmsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailingDetails whereSmsMailingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailingDetails whereStatusCode($value)
 * @mixin \Eloquent
 */
class SmsMailingDetails extends Model
{

    protected $table = 'sms-mailing-details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function smsMailing()
    {
        return $this->belongsTo(SmsMailing::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}