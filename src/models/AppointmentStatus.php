<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Mylid\SharedModels\AppointmentStatus
 *
 * @property int $id
 * @property string $name
 * @property string $color
 * @property string $slug
 * @property-read Collection|Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @method static Builder|AppointmentStatus newModelQuery()
 * @method static Builder|AppointmentStatus newQuery()
 * @method static Builder|AppointmentStatus query()
 * @method static Builder|AppointmentStatus whereColor($value)
 * @method static Builder|AppointmentStatus whereId($value)
 * @method static Builder|AppointmentStatus whereName($value)
 * @method static Builder|AppointmentStatus whereSlug($value)
 * @mixin \Eloquent
 */
class AppointmentStatus extends Model
{

    protected $table = 'appointment-status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * Get the user that owns the phone.
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}