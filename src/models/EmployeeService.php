<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Mylid\SharedModels\EmployeeService
 *
 * @property int $id
 * @property int $employee_id
 * @property int $service_id
 * @property int $company_id
 * @property int $price
 * @property int $duration
 * @property int $book_step
 * @method static Builder|EmployeeService newModelQuery()
 * @method static Builder|EmployeeService newQuery()
 * @method static Builder|EmployeeService query()
 * @method static Builder|EmployeeService whereBookStep($value)
 * @method static Builder|EmployeeService whereCompanyId($value)
 * @method static Builder|EmployeeService whereDuration($value)
 * @method static Builder|EmployeeService whereEmployeeId($value)
 * @method static Builder|EmployeeService whereId($value)
 * @method static Builder|EmployeeService wherePrice($value)
 * @method static Builder|EmployeeService whereServiceId($value)
 */
class EmployeeService extends Pivot
{
    protected $table = 'employees_services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}