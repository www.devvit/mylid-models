<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Model;

/**
 * Mylid\Database\SmsMailing
 *
 * @property int $id
 * @property string $title
 * @property string $msg
 * @property int $count
 * @property int $price
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $company_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mylid\Database\SmsMailingDetails[] $smsMailingDetails
 * @property-read int|null $sms_mailing_details_count
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailing newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailing newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailing query()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailing whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailing whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailing whereMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailing wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsMailing whereTitle($value)
 * @mixin \Eloquent
 */
class SmsMailing extends Model
{

    protected $table = 'sms-mailing';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    public function smsMailingDetails()
    {
        return $this->hasMany(SmsMailingDetails::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}