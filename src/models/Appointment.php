<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Mylid\SharedModels\Appointment
 *
 * @property int $id
 * @property string $date_created
 * @property int $employee_id
 * @property int $service_id
 * @property int $client_id
 * @property string $client_name
 * @property string $client_phone
 * @property string|null $client_email
 * @property string|null $client_comment
 * @property string|null $manager_comment
 * @property string $start_time
 * @property string $end_time
 * @property int|null $cash_sum
 * @property int|null $card_sum
 * @property int $price_expected
 * @property int|null $price_full
 * @property int|null $discount
 * @property string|null $discount_type
 * @property string|null $discount_paid_by
 * @property int|null $price_final
 * @property int $status_id
 * @property string|null $paid_by
 * @property int $company_id
 * @property-read Client $client
 * @property-read Company $company
 * @property-read Employee $employee
 * @property-read Service $service
 * @property-read AppointmentStatus $status
 * @method static Builder|Appointment newModelQuery()
 * @method static Builder|Appointment newQuery()
 * @method static Builder|Appointment query()
 * @method static Builder|Appointment whereCardSum($value)
 * @method static Builder|Appointment whereCashSum($value)
 * @method static Builder|Appointment whereClientComment($value)
 * @method static Builder|Appointment whereClientEmail($value)
 * @method static Builder|Appointment whereClientId($value)
 * @method static Builder|Appointment whereClientName($value)
 * @method static Builder|Appointment whereClientPhone($value)
 * @method static Builder|Appointment whereCompanyId($value)
 * @method static Builder|Appointment whereDateCreated($value)
 * @method static Builder|Appointment whereDiscount($value)
 * @method static Builder|Appointment whereDiscountPaidBy($value)
 * @method static Builder|Appointment whereDiscountType($value)
 * @method static Builder|Appointment whereEmployeeId($value)
 * @method static Builder|Appointment whereEndTime($value)
 * @method static Builder|Appointment whereId($value)
 * @method static Builder|Appointment whereManagerComment($value)
 * @method static Builder|Appointment wherePaidBy($value)
 * @method static Builder|Appointment wherePriceExpected($value)
 * @method static Builder|Appointment wherePriceFinal($value)
 * @method static Builder|Appointment wherePriceFull($value)
 * @method static Builder|Appointment whereServiceId($value)
 * @method static Builder|Appointment whereStartTime($value)
 * @method static Builder|Appointment whereStatusId($value)
 * @mixin \Eloquent
 */
class Appointment extends Model
{

    protected $table = 'appointments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * Get the user that owns the phone.
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function status()
    {
        return $this->belongsTo(AppointmentStatus::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}