<?php

namespace Mylid\SharedModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Mylid\SharedModels\Client
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string|null $email
 * @property string|null $password
 * @property int|null $discount
 * @property int $company_id
 * @property Carbon $created_at
 * @property-read Collection|Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @property-read Company $company
 * @property-read Collection|SmsMailingDetails[] $smsMailingDetails
 * @property-read int|null $sms_mailing_details_count
 * @method static Builder|Client newModelQuery()
 * @method static Builder|Client newQuery()
 * @method static Builder|Client query()
 * @method static Builder|Client whereCompanyId($value)
 * @method static Builder|Client whereCreatedAt($value)
 * @method static Builder|Client whereDiscount($value)
 * @method static Builder|Client whereEmail($value)
 * @method static Builder|Client whereId($value)
 * @method static Builder|Client whereName($value)
 * @method static Builder|Client wherePassword($value)
 * @method static Builder|Client wherePhone($value)
 * @mixin \Eloquent
 */
class Client extends Model
{
    protected $table = 'clients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    /**
     * Get the user that owns the phone.
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function smsMailingDetails()
    {
        return $this->hasMany(SmsMailingDetails::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //   'phone_verified_at' => 'datetime',
    ];
}