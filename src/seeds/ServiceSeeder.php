<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceSeeder extends Seeder
{
    public function run()
    {
        DB::table('services')->insert([
            'id' => 1,
            'name' => 'Маникюр',
            'company_id' => 1
        ]);

        DB::statement("ALTER TABLE `services` AUTO_INCREMENT = 2");
    }
}
