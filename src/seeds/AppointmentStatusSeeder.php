<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AppointmentStatusSeeder extends Seeder
{
    public function run()
    {
        DB::table('appointment-status')->insert([
            ['id' => 1, 'name' => 'Создан', 'slug' => 'created', 'color' => '#245bef'],
            ['id' => 2, 'name' => 'Не дозвонились', 'slug' => 'not-reached', 'color' => 'rgb(210, 150, 14)'],
            ['id' => 3, 'name' => 'Подтвержден', 'slug' => 'confirmed', 'color' => 'purple'],
            ['id' => 4, 'name' => 'Отменен', 'slug' => 'canceled', 'color' => 'rgb(113, 113, 113)'],
            ['id' => 5, 'name' => 'Клиент не пришел', 'slug' => 'not-came', 'color' => 'rgb(113, 113, 113)'],
            ['id' => 6, 'name' => 'Завершен', 'slug' => 'finished', 'color' => '#278227']
        ]);

        DB::statement("ALTER TABLE `appointment-status` AUTO_INCREMENT = 7");
    }
}
