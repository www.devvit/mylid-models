<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ClientSeeder extends Seeder
{
    public function run()
    {
        DB::table('clients')->insert([
            'id' => 1,
            'name' => 'Виталий',
            'phone' => '79502892110',
            'password' => Hash::make('123123'),
            'company_id' => 1,
            'discount' => 5,
            'created_at' => '2020-05-31 15:07:37'
        ]);
        DB::statement("ALTER TABLE `clients` AUTO_INCREMENT = 2");
    }
}
