<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobSeeder extends Seeder
{
    public function run()
    {
        DB::table('jobs')->insert([
            'id' => 1,
            'name' => 'Мастер по маникюру',
            'company_id' => 1
        ]);

        DB::statement("ALTER TABLE `jobs` AUTO_INCREMENT = 2");
    }
}
