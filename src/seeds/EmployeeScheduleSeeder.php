<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeScheduleSeeder extends Seeder
{
    public function run()
    {
        DB::table('employee-schedules')
            ->insert([
                [
                    'schedule_date' => '2020-05-25',
                    'time_start' => '09:00:00',
                    'time_end' => '18:00:00',
                    'employee_id' => 1,
                    'company_id' => 1
                ],
                [
                    'schedule_date' => '2020-05-27',
                    'time_start' => '09:00:00',
                    'time_end' => '18:00:00',
                    'employee_id' => 1,
                    'company_id' => 1
                ],
                [
                    'schedule_date' => '2020-05-30',
                    'time_start' => '09:00:00',
                    'time_end' => '18:00:00',
                    'employee_id' => 1,
                    'company_id' => 1
                ],


                [
                    'schedule_date' => '2020-05-25',
                    'time_start' => '12:00:00',
                    'time_end' => '16:00:00',
                    'employee_id' => 2,
                    'company_id' => 1
                ],
                [
                    'schedule_date' => '2020-05-26',
                    'time_start' => '12:00:00',
                    'time_end' => '16:00:00',
                    'employee_id' => 2,
                    'company_id' => 1
                ],
                [
                    'schedule_date' => '2020-05-27',
                    'time_start' => '12:00:00',
                    'time_end' => '16:00:00',
                    'employee_id' => 2,
                    'company_id' => 1
                ],
                [
                    'schedule_date' => '2020-05-28',
                    'time_start' => '12:00:00',
                    'time_end' => '16:00:00',
                    'employee_id' => 2,
                    'company_id' => 1
                ],
                [
                    'schedule_date' => '2020-05-29',
                    'time_start' => '12:00:00',
                    'time_end' => '16:00:00',
                    'employee_id' => 2,
                    'company_id' => 1
                ],


                [
                    'schedule_date' => '2020-05-25',
                    'time_start' => '09:00:00',
                    'time_end' => '15:00:00',
                    'employee_id' => 3,
                    'company_id' => 1
                ],
                [
                    'schedule_date' => '2020-05-27',
                    'time_start' => '09:00:00',
                    'time_end' => '15:00:00',
                    'employee_id' => 3,
                    'company_id' => 1
                ],
                [
                    'schedule_date' => '2020-05-29',
                    'time_start' => '09:00:00',
                    'time_end' => '15:00:00',
                    'employee_id' => 3,
                    'company_id' => 1
                ],
            ]);
    }
}
