<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeServiceSeeder extends Seeder
{
    public function run()
    {
        DB::table('employees_services')->insert([
            'id' => 1,
            'employee_id' => 1,
            'service_id' => 1,
            'price' => 100000,
            'duration' => 120,
            'book_step' => 120,
            'company_id' => 1
        ]);

        DB::statement("ALTER TABLE `employees_services` AUTO_INCREMENT = 2");
    }
}
