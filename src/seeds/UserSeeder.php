<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('owners')->insert([
            [
                'id' => 1,
                'created_at' => '2020-05-31 12:27:05',
                'user_id' => 1,
            ]
        ]);
        DB::statement("ALTER TABLE `owners` AUTO_INCREMENT = 2");

        DB::table('businesses')->insert([
            'id' => 1,
            'name' => 'Салон красоты',
            'slug' => Str::slug('Салон красоты'),
            'created_at' => '2020-05-14 11:25:05'
        ]);
        DB::statement("ALTER TABLE `businesses` AUTO_INCREMENT = 2");

        DB::table('companies')->insert([
            'id' => 1,
            'name' => 'ООО Главсофт',
            'slug' => Str::slug('ООО Главсофт'),
            'open_at' => '09:00:00',
            'close_at' => '18:00:00',
            'timezone' => 'Asia/Vladivostok',
            'paid_until' => Carbon::now('UTC')->addDays(30)->toDateTimeString(),
            'pricing_plan_id' => 1,
            'business_id' => 1,
            'owner_id' => 1,
            'created_at' => '2020-05-14 11:25:05'
        ]);
        DB::statement("ALTER TABLE `companies` AUTO_INCREMENT = 2");

        DB::table('payment-history')->insert([
            'id' => 1,
            'owner_id' => 1,
            'company_id' => 1,
            'info' => 'Покупка тарифа "Пробный"',
            'amount' => 0,
            'paid_at' => '2020-06-27 11:00:00',
            'status_id' => 1,
            'created_at' => '2020-06-27 11:00:00'
        ]);
        DB::statement("ALTER TABLE `payment-history` AUTO_INCREMENT = 2");
    }
}
