<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PricingPlanSeeder extends Seeder
{
    public function run()
    {
        DB::table('pricing-plans')->insert([
            [
                'id' => 1,
                'name' => 'Пробный',
                'slug' => Str::slug('Пробный'),
                'price_1month' => 0,
                'price_3month' => 0,
                'price_6month' => 0,
                'price_12month' => 0,
                'created_at' => '2020-06-25 15:00:00'
            ],
            [
                'id' => 2,
                'name' => 'Начальный',
                'slug' => Str::slug('Начальный'),
                'price_1month' => 70000,
                'price_3month' => 65000,
                'price_6month' => 60000,
                'price_12month' => 50000,
                'created_at' => '2020-06-25 15:00:00'
            ],
            [
                'id' => 3,
                'name' => 'Стандартный',
                'slug' => Str::slug('Стандартный'),
                'price_1month' => 80000,
                'price_3month' => 75000,
                'price_6month' => 70000,
                'price_12month' => 60000,
                'created_at' => '2020-06-25 15:00:00'
            ],
            [
                'id' => 4,
                'name' => 'Pro',
                'slug' => Str::slug('Pro'),
                'price_1month' => 100000,
                'price_3month' => 95000,
                'price_6month' => 90000,
                'price_12month' => 80000,
                'created_at' => '2020-06-25 15:00:00'
            ]
        ]);
        DB::statement("ALTER TABLE `pricing-plans` AUTO_INCREMENT = 5");
    }
}
