<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmployeeSeeder extends Seeder
{
    public function run()
    {
        DB::table('employees')->insert([
            [
                'id' => 1,
                'company_id' => 1,
                'phone' => '79502892110',
                'firstname' => 'Галь',
                'lastname' => 'Гадот',
                'birth' => '1985-04-30',
            ],
            [
                'id' => 2,
                'company_id' => 1,
                'phone' => '79502892111',
                'firstname' => 'Рассел',
                'lastname' => 'Кроу',
                'birth' => '1964-04-07',
            ],
            [
                'id' => 3,
                'company_id' => 1,
                'phone' => '79502892112',
                'firstname' => 'Киану',
                'lastname' => 'Ривз',
                'birth' => '1964-09-02',
            ],
        ]);

        DB::statement("ALTER TABLE `employees` AUTO_INCREMENT = 4");
    }
}
