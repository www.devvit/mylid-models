<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PricingPlanSeeder::class,
            UserSeeder::class,
            ClientSeeder::class,
            AppointmentStatusSeeder::class,
            EmployeeSeeder::class,
            EmployeeScheduleSeeder::class,
            JobSeeder::class,
            EmployeeJobSeeder::class,
            ServiceSeeder::class,
            EmployeeServiceSeeder::class
        ]);
    }
}
