<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AppointmentSeeder extends Seeder
{
    public function run()
    {
        DB::table('appointments')->insert([
            [
                'id' => 1,
                'date_created' => '2020-05-01',
                'employee_id' => 1,
                'client_id' => 1,
                'client_name' => 'Валентина',
                'client_phone' => '79023584898',
                'client_comment' => 'Опоздаю на 15 минут',
                'manager_comment' => 'Клиент всё понравилось',
                'service_id' => 2,
                'start_time' => '2020-05-04 23:00:00',
                'end_time' => '2020-05-05 01:00:00',
                'discount' => 10,
                'price_expected' => 150000,
                'price_full' => 150000,
                'price_final' => 135000,
                'paid_by' => 'cash',
                'status_id' => 3,
                'company_id' => 1
            ],
            [
                'id' => 2,
                'date_created' => '2020-05-02',
                'employee_id' => 1,
                'client_id' => 1,
                'client_name' => 'Ольга',
                'client_phone' => '79023584898',
                'client_comment' => null,
                'manager_comment' => null,
                'service_id' => 2,
                'start_time' => '2020-05-05 06:15:00',
                'end_time' => '2020-05-05 08:00:00',
                'discount' => null,
                'price_expected' => 150000,
                'price_full' => 150000,
                'price_final' => 150000,
                'paid_by' => null,
                'status_id' => 5,
                'company_id' => 1
            ],
            [
                'id' => 3,
                'date_created' => '2020-05-03',
                'employee_id' => 1,
                'client_id' => 1,
                'client_name' => 'Светлана',
                'client_phone' => '79023584898',
                'client_comment' => null,
                'manager_comment' => null,
                'service_id' => 2,
                'start_time' => '2020-05-06 06:45:00',
                'end_time' => '2020-05-06 07:45:00',
                'discount' => null,
                'price_expected' => 150000,
                'price_full' => 150000,
                'price_final' => 150000,
                'paid_by' => 'card',
                'status_id' => 6,
                'company_id' => 1
            ]
        ]);
    }
}
