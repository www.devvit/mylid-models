<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeJobSeeder extends Seeder
{
    public function run()
    {
        DB::table('employees_jobs')
            ->insert([
                ['employee_id' => 1, 'job_id' => 1],
                ['employee_id' => 2, 'job_id' => 1],
                ['employee_id' => 3, 'job_id' => 1]
            ]);
    }
}
