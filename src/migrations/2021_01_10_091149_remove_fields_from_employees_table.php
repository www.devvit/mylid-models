<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFieldsFromEmployeesTable extends Migration
{
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn([
                'password',
                'token',
                'active',
            ]);
        });
    }

    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->string('password', 255)->nullable();
            $table->string('token', 255);
            $table->boolean('active')->default(0);
        });
    }
}
