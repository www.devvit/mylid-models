<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsCodesTable extends Migration
{
    public function up()
    {
        Schema::create('sms_codes', function (Blueprint $table) {
            $table->id();
            $table->string('phone', 20);
            $table->string('code', 20);
            $table->timestamp('sent_at')->useCurrent();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sms_codes');
    }
}
