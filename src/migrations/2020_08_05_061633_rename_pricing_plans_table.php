<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class RenamePricingPlansTable extends Migration
{
    public function up()
    {
        Schema::rename('pricing_plans', 'pricing-plans');
    }

    public function down()
    {
        Schema::rename('pricing-plans', 'pricing_plans');
    }
}
