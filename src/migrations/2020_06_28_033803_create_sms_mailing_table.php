<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsMailingTable extends Migration
{
    public function up()
    {
        Schema::create('sms-mailing', function (Blueprint $table) {
            $table->id();
            $table->string('title', 255);
            $table->string('msg', 255);
            $table->unsignedInteger('count');
            $table->unsignedInteger('price');
            $table->dateTime('created_at');
            $table->unsignedBigInteger('company_id');

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sms-mailing');
    }
}
