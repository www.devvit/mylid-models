<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnersTable extends Migration
{
    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable();
            $table->decimal('phone', 20, 0)->nullable()->unique();
            $table->string('email', 255)->nullable()->unique();
            $table->string('password', 255)->nullable();
            $table->date('birth')->nullable();
            $table->string('img_path', 500)->nullable();
            $table->string('img_name', 255)->nullable();
            $table->unsignedBigInteger('balance')->default(0);
            $table->boolean('email_confirmed')->default(0);
            $table->boolean('phone_confirmed')->default(0);
            $table->boolean('active')->default(false);
            $table->datetime('created_at')->useCurrent();
        });
    }

    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
