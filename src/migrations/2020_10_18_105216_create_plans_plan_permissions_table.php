<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansPlanPermissionsTable extends Migration
{
    public function up()
    {
        Schema::create('plans_plan-permissions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plan_id');
            $table->unsignedBigInteger('permission_id');

            $table->unique(['plan_id', 'permission_id']);
            $table->foreign('plan_id')->references('id')->on('pricing-plans')->onDelete("cascade");
            $table->foreign('permission_id')->references('id')->on('plan-permissions');
        });
    }

    public function down()
    {
        Schema::dropIfExists('plans_plan-permissions');
    }
}
