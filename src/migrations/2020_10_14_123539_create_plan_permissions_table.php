<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanPermissionsTable extends Migration
{
    public function up()
    {
        Schema::create('plan-permissions', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->unique();
        });
    }

    public function down()
    {
        Schema::dropIfExists('plan-permissions');
    }
}
