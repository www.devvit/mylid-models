<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesJobsTable extends Migration
{
    public function up()
    {
        Schema::create('employees_jobs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('job_id');

            $table->foreign('employee_id')->references('id')->on('employees')->onDelete("cascade");
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete("cascade");
            $table->unique(['employee_id', 'job_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('employees_jobs');
    }
}
