<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricingPlansTable extends Migration
{
    public function up()
    {
        Schema::create('pricing_plans', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->unsignedInteger('price_1month')->nullable();
            $table->unsignedInteger('price_3month')->nullable();
            $table->unsignedInteger('price_6month')->nullable();
            $table->unsignedInteger('price_12month')->nullable();
            $table->dateTime('created_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('pricing_plans');
    }
}
