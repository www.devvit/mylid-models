<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFieldsFromOwnersTable extends Migration
{
    public function up()
    {
        Schema::table('owners', function (Blueprint $table) {
            $table->dropColumn([
                'firstname',
                'lastname',
                'phone',
                'email',
                'password',
                'birth',
                'img_path',
                'img_name',
                'balance',
                'email_confirmed',
                'phone_confirmed',
                'active',
            ]);
        });
    }

    public function down()
    {
        Schema::table('owners', function (Blueprint $table) {
            $table->string('firstname', 255)->after('id');
            $table->string('lastname', 255)->after('firstname');
            $table->decimal('phone', 20, 0)->nullable()->unique();
            $table->string('email', 255)->nullable()->unique();
            $table->string('password', 255)->nullable();
            $table->date('birth')->nullable();
            $table->string('img_path', 500)->nullable();
            $table->string('img_name', 255)->nullable();
            $table->unsignedBigInteger('balance')->default(0);
            $table->boolean('email_confirmed')->default(0);
            $table->boolean('phone_confirmed')->default(0);
            $table->boolean('active')->default(false);
        });
    }
}
