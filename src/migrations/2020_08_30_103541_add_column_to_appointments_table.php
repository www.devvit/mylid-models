<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToAppointmentsTable extends Migration
{
    public function up()
    {
        Schema::table('appointments', function (Blueprint $table) {
            $table->enum('discount_type', ['number', 'percent'])->nullable()->after('discount');
            $table->enum('discount_paid_by', ['company', 'employee'])->nullable()->after('discount_type');
        });
    }

    public function down()
    {
        Schema::table('appointments', function (Blueprint $table) {
            $table->dropColumn('discount_type');
            $table->dropColumn('discount_paid_by');
        });
    }
}
