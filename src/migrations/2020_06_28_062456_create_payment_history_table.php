<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentHistoryTable extends Migration
{
    public function up()
    {
        Schema::create('payment-history', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('owner_id');
            $table->unsignedBigInteger('company_id');
            $table->string('info');
            
            $table->unsignedInteger('amount');
            $table->dateTime('paid_at')->nullable();
            $table->unsignedSmallInteger('status_id');
            $table->dateTime('created_at');

            $table->foreign('owner_id')->references('id')->on('owners')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('payment-history');
    }
}
