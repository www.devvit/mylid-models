<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->unique();
            $table->string('slug', 255)->unique();
            $table->time('open_at', 0)->nullable();
            $table->time('close_at', 0)->nullable();
            $table->string('timezone', 50)->nullable();
            $table->unsignedBigInteger('business_id');
            $table->unsignedBigInteger('owner_id');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('business_id')->references('id')->on('businesses');
            $table->foreign('owner_id')->references('id')->on('owners')->onDelete("cascade");
        });
    }

    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
