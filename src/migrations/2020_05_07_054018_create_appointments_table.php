<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date_created', 0)->useCurrent();
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('service_id');
            $table->unsignedBigInteger('client_id');
            $table->string('client_name', 255);
            $table->string('client_phone', 255);
            $table->string('client_email', 255)->nullable();
            $table->string('client_comment', 255)->nullable();
            $table->string('manager_comment', 255)->nullable();
            $table->dateTime('start_time');
            $table->dateTime('end_time');

            $table->unsignedInteger('cash_sum')->nullable();
            $table->unsignedInteger('card_sum')->nullable();

            $table->unsignedInteger('price_expected');
            $table->unsignedInteger('price_full')->nullable();
            $table->unsignedTinyInteger('discount')->nullable();
            $table->unsignedInteger('price_final')->nullable();
            $table->unsignedBigInteger('status_id');
            $table->enum('paid_by', ['cash', 'card', 'mix'])->nullable();
            $table->unsignedBigInteger('company_id');
            

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('appointment-status')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
