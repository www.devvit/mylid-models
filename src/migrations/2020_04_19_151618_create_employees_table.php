<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id');

            $table->decimal('phone', 20, 0)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('password', 255)->nullable();

            $table->string('firstname', 255);
            $table->string('lastname', 255);
            $table->string('middlename', 255)->nullable();
            $table->date('birth');
            $table->string('img_path', 500)->nullable();
            $table->string('img_name', 255)->nullable();
            $table->string('token', 255);
            $table->boolean('active')->default(0);

            $table->foreign('company_id')->references('id')->on('companies');

            $table->unique(['phone', 'company_id']);
            $table->unique(['email', 'company_id']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
