<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCompaniesTable extends Migration
{
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dateTime('paid_until')->nullable();
            $table->unsignedBigInteger('pricing_plan_id')->nullable();

            $table->foreign('pricing_plan_id')->references('id')->on('pricing_plans');
        });
    }

    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropForeign('companies_pricing_plan_id_foreign');
            $table->dropColumn('paid_until');
            $table->dropColumn('pricing_plan_id');
        });
    }
}
