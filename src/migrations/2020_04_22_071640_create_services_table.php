<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('description', 255)->nullable();
            $table->unsignedBigInteger('group_id')->nullable();
            $table->unsignedBigInteger('company_id');

            $table->foreign('group_id')->references('id')->on('service-groups')->onUpdate("cascade");
            $table->foreign('company_id')->references('id')->on('companies')->onDelete("cascade");
            $table->unique(['name', 'company_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('services');
    }
}
