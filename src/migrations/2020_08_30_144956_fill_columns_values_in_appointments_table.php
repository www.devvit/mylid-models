<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillColumnsValuesInAppointmentsTable extends Migration
{
    public function up()
    {
        DB::table('appointments')
            ->where([
                ['discount', '<>', null],
                ['discount', '<>', 0]
            ])
            ->update([
                'discount_type' => 'percent',
                'discount_paid_by' => 'employee'
            ]);
    }

    public function down()
    {
        DB::table('appointments')
            ->where([
                ['discount', '<>', null],
                ['discount', '<>', 0]
            ])
            ->update([
                'discount_type' => null,
                'discount_paid_by' => null
            ]);
    }
}
