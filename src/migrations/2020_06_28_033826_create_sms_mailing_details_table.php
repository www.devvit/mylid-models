<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsMailingDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('sms-mailing-details', function (Blueprint $table) {
            $table->id();
            $table->string('sms_id', 255)->nullable()->unique();
            $table->unsignedBigInteger('client_id')->nullable();
            $table->string('phone', 20);
            $table->unsignedSmallInteger('status_code')->nullable();
            $table->unsignedBigInteger('sms_mailing_id');

            $table->foreign('sms_mailing_id')->references('id')->on('sms-mailing')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sms-mailing-details');
    }
}
