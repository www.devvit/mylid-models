<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('name', 20);
            $table->decimal('phone', 20, 0);
            $table->string('email', 255)->nullable();
            $table->string('password', 255)->nullable();
            $table->tinyInteger('discount')->nullable();
            $table->unsignedBigInteger('company_id');
            $table->datetime('created_at')->useCurrent();

            $table->unique(['phone', 'company_id']);
            $table->unique(['email', 'company_id']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete("cascade");
        });
    }

    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
