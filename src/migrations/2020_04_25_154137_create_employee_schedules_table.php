<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeSchedulesTable extends Migration
{
    public function up()
    {
        Schema::create('employee-schedules', function (Blueprint $table) {
            $table->id();
            $table->date('schedule_date');
            $table->time('time_start');
            $table->time('time_end');
            $table->time('break_start_time')->nullable();
            $table->time('break_end_time')->nullable();
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('company_id');

            $table->foreign('employee_id')->references('id')->on('employees')->onDelete("cascade");
            $table->foreign('company_id')->references('id')->on('companies');
            $table->unique(['schedule_date', 'employee_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('employee-schedules');
    }
}
