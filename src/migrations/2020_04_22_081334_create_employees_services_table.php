<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesServicesTable extends Migration
{
    public function up()
    {
        Schema::create('employees_services', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('service_id');
            $table->unsignedBigInteger('company_id');

            $table->unsignedBigInteger('price');
            $table->unsignedSmallInteger('duration');
            $table->unsignedSmallInteger('book_step');

            $table->foreign('employee_id')->references('id')->on('employees')->onDelete("cascade");
            $table->foreign('service_id')->references('id')->on('services')->onDelete("cascade");
            $table->foreign('company_id')->references('id')->on('companies')->onDelete("cascade");
            $table->unique(['employee_id', 'service_id', 'company_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('employees_services');
    }
}
